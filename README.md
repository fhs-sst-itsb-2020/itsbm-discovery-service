# Service Discovery
 
To enable the eureka client in your spring application, add the following dependency:

### Gradle:

```
implementation 'org.springframework.cloud:spring-cloud-starter-netflix-eureka-client'
```

### Maven:

```
<dependency>
    <groupId>org.springframework.cloud</groupId>
		    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
```

## Your application needs:

```
@EnableEurekaClient  // <-- this
@SpringBootApplication
public class YourSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(YourSpringApplication.class, args);
    }
}

```

Last but not least, you have to include one of the lines below in your:

## application.properties: 
```
eureka.client.serviceUrl.defaultZone=http://${EUREKA_HOST:localhost:8761}/eureka
```
## application.yml:
```
eureka:
    client:
        serviceUrl:
            defaultZone: http://${EUREKA_HOST:localhost:8761}/eureka
 ```
